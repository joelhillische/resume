Rails.application.routes.draw do
  resources :dashboards, path: 'dashboard'
  resources :requests
  resources :sessions, path: 'login'

  root to: 'requests#index'
end
