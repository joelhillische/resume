class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  
  helper_method :current_user

  def authenticated?
    redirect_to sessions_url, alert: "Please login, and you'll be sent to the page you tried to access." if current_user.nil?
  end

  def current_user
    return unless session[:user_id]
    @current_user ||= User.find_by(id: session[:user_id]) 
  end
end
