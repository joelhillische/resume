class SessionsController < ApplicationController
  before_filter :authenticated?, except: [:index, :create]

  def index
    redirect_to dashboards_path unless current_user.nil?
  end

  def create
    user = User.find_by(email: params[:email].downcase, pin: params[:pin])
    if user 
      if user.noaws?
        aa = AmazonAccount.new
        aa.create(user)
      end
      session[:user_id] = user.id
      redirect_to dashboards_path, notice: "Welcome! Future employer or co-worker!"
    else
      flash[:alert] = "Incorrect email address or pin"
      render "index"
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to sessions_path
  end
end
