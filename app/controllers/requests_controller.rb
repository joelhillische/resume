class RequestsController < ApplicationController
  def index
    redirect_to dashboards_path if current_user
  end

  def create
    user = User.new(email: params[:email])

    if user.save
      user.randompin
      #AmazonAccount.create(user)
      RequestMailer.delay.confirmation(user)
      redirect_to sessions_path, notice: "An e-mail has been sent to you containing your PIN"
    else
      flash.now[:alerts] = user.errors.full_messages
      render "index"
    end
  end
end
