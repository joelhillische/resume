class RequestMailer < ApplicationMailer
  default from: "joel@joelhill-ische.com"
  default bcc: "joel@joelhill-ische.com"

  def confirmation(user)
    @user = user
    mail to: @user.email, subject: "Username and PIN"
  end
end
