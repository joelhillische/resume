class User < ApplicationRecord
  include AASM

  validates :email, uniqueness: true, presence: true

  aasm do # default column: aasm_state
    state :noaws, :initial => true
    state :aws

    event :add do
      transitions :to => :aws
    end

    event :reset do
      transitions :to => :noaws
    end
  end

  def randompin
    self.update_column(:pin, rand(1000..9999))
  end
end
