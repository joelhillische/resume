class AmazonAccount
  require 'aws-sdk'
  require 'securerandom'

  def create(user)
    client = Aws::IAM::Client.new(region: 'us-east-1', access_key_id: Rails.application.secrets.app_rw_access_key_id, secret_access_key: Rails.application.secrets.app_rw_secret_access_key)
    tempusername = "#{user.email}"
    temppassword = SecureRandom.urlsafe_base64(8)
    
    begin user_resp = client.get_user({user_name: tempusername})
      resp = client.create_access_key({user_name: tempusername})
    rescue Aws::IAM::Errors::NoSuchEntity
      user_resp = client.create_user(user_name: tempusername)
      client.create_login_profile(user_name: tempusername, password: temppassword)
      client.add_user_to_group({group_name: "ReadOnlyGroup", user_name: tempusername})
      resp = client.create_access_key({user_name: tempusername})
    end

    user.amazon_username = user_resp.user.user_name if user.amazon_username.nil?
    user.amazon_password = temppassword if user.amazon_password.nil?
    user.access_key_id = resp.access_key.access_key_id if user.access_key_id.nil?
    user.secret_access_key = resp.access_key.secret_access_key if user.secret_access_key.nil?
    user.save

    # We change the user state from noaws to aws meaning that the aws information has been created
    user.add!
  end
end
