class AddAmazonUsernameToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :amazon_username, :string
  end
end
