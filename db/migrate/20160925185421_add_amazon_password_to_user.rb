class AddAmazonPasswordToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :amazon_password, :string
  end
end
