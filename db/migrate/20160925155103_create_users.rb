class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    drop_table :users, if_exists: true
    create_table :users do |t|
      t.string :email
      t.integer :pin
      t.string :access_key_id
      t.string :secret_access_key
      t.string :slug

      t.timestamps
    end
  end
end
