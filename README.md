# README

Resume site that demonstrates the ability of Joel Hill to create a AWS infrastructure using ELB, AutoScaling, EC2, S3, IAM users, and automatic creation of read only users in the AWS platform using a Ruby SDK

Things you may want to cover:

* Ruby version - 2.3.1

* AWS PostgreSQL RDB

* Delayed Jobs

